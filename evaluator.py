
import csv
import numpy as np
import gzip
import os
import torch
from transformers import *
from elasticsearch import Elasticsearch
import pytrec_eval
import pandas as pd
from bert_serving.client import BertClient
from tokenizers import BertWordPieceTokenizer


class Evaluator:
    def __init__(self):
        self.__client = Elasticsearch(
            [{"host": "ha-opendistro-es-discovery.opendistro", "port": 9200}]
        )
        # pod_index = os.getenv('HOSTNAME').split('-')[-1]
        pod_index = 0
        host = "bert.production"
        pod = "bert"
        pod_name = f"{pod}-{pod_index}"
        host_name = f"{pod_name}.{host}"
        print(host_name)
        try:
            self.__bert = BertClient(
                ip=host_name,
                port=5555,
                port_out=5556,
                check_version=True,
                check_length=False,
                timeout=60000,
            )
        except Exception as e:
            print("exception bro")
        else:
            print("worked bro")
        self.__tokenizer = BertWordPieceTokenizer("vocab.txt", lowercase=True)

    def eval(self):
        qrel = self.text_rels()
        run, df = self.read_dl_topics(qrel)

        metrics = {
            "map": 0.0,
            "ndcg_cut_10": 0.0,
            "ndcg_cut_100": 0.0,
            "recall_10": 0.0,
            "recall_100": 0.0,
            "recall_1000": 0.0,
            "recip_rank": 0.0,
        }

        evaluator = pytrec_eval.RelevanceEvaluator(qrel, pytrec_eval.supported_measures)
        # res = evaluator.evaluate(run)

        # for r in res:
        #    for metric in metrics:
        #        metrics[metric] += res[r][metric]

        return 0, 1, df

    @staticmethod
    def text_rels():
        qrel = {}
        with open("qrels.dl19-doc.txt", "r") as f:
            lines = [line.rstrip("\n") for line in f]
            for line in lines:
                split = line.strip().split()

                topic = split[0]
                docid = split[2]
                rel = split[3]

                if topic not in qrel:
                    qrel[topic] = {}
                qrel[topic][docid] = int(rel)

        return qrel

    def read_dl_topics(self, qrel, size=1000):
        results = {}
        datas = []
        # tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')
        # model = AutoModel.from_pretrained('bert-base-uncased')
        columns = ["Query", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
        result_df = pd.DataFrame(columns=columns)
        with open("topics.dl19-doc.txt", "r") as f:
            lines = [line.rstrip("\n") for line in f]
            for line in lines:
                split = line.strip().split("\t")

                topic = split[0]
                q = split[1]

                results[topic] = {}
                data = self.__tokenizer.encode(q)
                q = self.__bert.encode(
                    [data.tokens[1:-1]], is_tokenized=True, show_tokens=False, blocking=True
                )
                print(data.offsets)
                print(data.tokens)
                print("len(q)")
                print(len(q[0]))
                qs = [q[0, : len(data.tokens)]]
                # qs = q[0][1:len(q[0]-1)]
                print("qs len")
                print(len(qs[0]))
                qq = np.concatenate(qs)[1:-1]
                print("lenQQ")
                print(len(qq[0]))
                q = np.average(qq, axis=0).tolist()
                # input_ids = torch.tensor(tokenizer.encode(q)).unsqueeze(0)
                # all_hidden_states, all_attentions = model(input_ids)[-2:]
                # q = all_hidden_states[0][-1].detach().tolist()
                query = {
                    "size": 10,
                    "query": {
                        "knn": {"representations.embedding": {"vector": q, "k": 10}}
                    },
                }

                res = self.__client.search(
                    size=size,
                    index="zetaalpha_trec_documents",
                    body=query,
                    _source=True,
                    request_timeout=600000,
                )
                count = 0
                data = []
                scores = []
                ids = []
                ids.append(split[1])
                scores.append(split[1])
                judgements = []
                judgements.append(split[1])
                data.append(split[1])
                for hit in res["hits"]["hits"]:
                    count += 1
                    if count > 10:
                        break
                    data.append(hit["_source"]["representations"]["text"][:500])
                    # data.append(hit["_source"]["metadata"]["DCMI.identifier"])
                    scores.append(hit["_score"])
                    ids.append(hit["_source"]["metadata"]["DCMI.identifier"])
                    # if qrel[split[1]][hit["_source"]["metadata"]["DCMI.identifier"] is not None:
                    try:
                        judgements.append(
                            qrel[split[0]][hit["_source"]["metadata"]["DCMI.identifier"]]
                        )
                    except KeyError:
                        judgements.append("0 - Not provided")
                result_df = result_df.append(pd.DataFrame(data=[data]))
                result_df = result_df.append(pd.DataFrame(data=[scores]))
                result_df = result_df.append(pd.DataFrame(data=[judgements]))
                result_df = result_df.append(pd.DataFrame(data=[ids]))
                # for hit in res["hits"]["hits"]:
                #    results[topic][hit["_source"]["metadata"]["DCMI.identifier"]] = hit[
                #       "_score"
                #   ]
        # count,unique = np.unique(datas,return_counts=True)
        # print(len(unique))
        # print(count)
        return results, result_df


